# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Rainbow pencil                                   |           | Y         |
| Hollow/Solid shape                               |           | Y         |


---

### How to use 
**◆ 使用介面**
\
![](https://i.imgur.com/GQ9KrsU.png)
\
**◆ 可用工具**
##### 1. 畫筆Pencil ![](https://i.imgur.com/vWhHq1n.png)
點選pencil功能，可在畫布上作畫，右方滑動條可調整pencil粗細。
##### 2. 彩虹筆Rainbow Pencil ![](https://i.imgur.com/ILWfeIS.png)
點選rainbow pencil功能，在作畫時畫筆顏色會隨著畫筆移動而改變，若要改變粗細則調整Pencil右方的滑動條。
##### 3. 橡皮擦eraser ![](https://i.imgur.com/U9f6ymk.png)
點選eraser功能，可在擦取畫布，右方滑動條可調整eraser粗細。
##### 4. 文字Type ![](https://i.imgur.com/uXmh0Am.png)
點選type功能，在畫布上方的text box輸入文字，並選擇字體、大小後，點擊欲在畫布上插入文字的位置，即可輸入文字。
##### 5. 圓圈Circle ![](https://i.imgur.com/BheD5fm.png)
 
點選circle功能，可在畫布上畫圓形，其中點選位置是圓圈的中心。
##### 6. 方形Rectangle ![](https://i.imgur.com/DYo7BZ5.png)
點選rectangle功能，可在畫布上畫方形，其中點選位置是方形的左上角。
##### 7. 三角形Triangle ![](https://i.imgur.com/F0v59nc.png)
點選triangle功能，可在畫布上畫三角形，其中點選位置是三角形上方的頂點。
##### 8. 空心Hollow/實心Solid ![](https://i.imgur.com/3iwk85N.png)
點選可交互切換hollow/solid功能。
預設值為hollow，在畫圓形、方形及三角形時僅會出現框線，若要改變框線粗細則調整Pencil右方的滑動條；若為solid，則畫圓形、方形及三角形時即為填滿狀態。
##### 9. 調色盤Color ![](https://i.imgur.com/y4m9RPu.png)
點選即可選擇顏色，其中畫筆、橡皮擦、文字、圓圈、方形及三角形都是使用此功能作顏色改變。
##### 10. 復原Undo ![](https://i.imgur.com/NMcPy4T.png)
點選undo功能，可復原上一個動作。
##### 11. 重做Redo ![](https://i.imgur.com/l4KxuiH.png)
點選redo功能，可重做下一個動作。
##### 12. 清除Clear ![](https://i.imgur.com/jL35yka.png)
點選clear功能，可清除整個畫布。
##### 13. 存圖Save ![](https://i.imgur.com/J2PnoE4.png)
點選save功能，可存取畫布至電腦，其中檔名為myCanvas。
##### 14. 上傳Upload ![](https://i.imgur.com/QbYiIMy.png)
點選upload功能，選取電腦中圖檔後，可將圖檔顯示在畫布上。

### Gitlab page link
[https://107062337.gitlab.io/AS_01_WebCanvas](https://107062337.gitlab.io/AS_01_WebCanvas )

<style>
table th{
    width: 100%;
}
</style>
