var draw;
var canvas, ctx;
var mouse_x, mouse_y;
var click_x, click_y;
var last_x, last_y;
var state;
var pen_weight, pen_color, erase_weight;
var center_x, center_y;
var word_size, word_form, text;
var redo=[];
var undo=[];
var temp;
var filled;
var hue = 0;


window.addEventListener("mousedown", event =>{
    click_x = event.offsetX;
    click_y = event.offsetY;
}
)

window.addEventListener("mousemove", event =>{
    mouse_x = event.offsetX;
    mouse_y = event.offsetY;
}
)

function init(){
    canvas = document.getElementById('canvas');
    ctx = canvas.getContext('2d');
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    filled = false;
    pen_weight = document.getElementById("PencilRange").value;
    erase_weight = document.getElementById("EraseRange").value;
}

function mousedown(){
    undo.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    redo=[];
    draw = true;
    ctx.beginPath();
    ctx.moveTo(mouse_x-1, mouse_y-1);
    last_x = mouse_x;
    last_y = mouse_y;
    if(state=="type"){
        ChangeWordAttribute();
        text = document.getElementById("words").value;
        ctx.fillText(text, event.offsetX, event.offsetY);
    }
}

function mousemove(){
    if(state=="pencil" && draw){
        ChangePencilArribute();
        ctx.lineTo(mouse_x, mouse_y);
        ctx.stroke();
    }
    else if(state=="rainbow" && draw){
        console.log(hue);
        if(hue == 360) hue = 0;
        else hue += 5;
        ctx.lineWidth = pen_weight;
        ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
        ctx.beginPath();
        ctx.moveTo(last_x, last_y);
        ctx.lineTo(event.offsetX, event.offsetY);
        ctx.stroke();
        last_x = event.offsetX;
        last_y = event.offsetY;
        ctx.closePath();
    }
    else if(state=="eraser" && draw){
        ctx.lineWidth = erase_weight;
        ctx.globalCompositeOperation = "destination-out";
        ctx.lineTo(mouse_x, mouse_y);
        ctx.stroke();
    }  
    else if(state=="circle" && draw){
        temp = undo.pop();
        ctx.putImageData(temp, 0, 0);
        undo.push(temp);
        drawEllipseByCenter(ctx, click_x, click_y, (mouse_x-click_x)*2, (mouse_y-click_y)*2);
    } 
    else if(state=="triangle" && draw){
        temp = undo.pop();
        ctx.putImageData(temp, 0, 0);
        undo.push(temp);
        drawTriangle(ctx, click_x, click_y, mouse_x, mouse_y);
    }
    else if(state=="rectangle" && draw){
        temp = undo.pop();
        ctx.putImageData(temp, 0, 0);
        undo.push(temp);
        drawRectangle(ctx, click_x, click_y, mouse_x, mouse_y);
    }
}

function mouseup(){
    draw = false;
    ctx.globalCompositeOperation = "source-over";
    ctx.closePath();
}

function mouseout(){
    draw = false;
    ctx.globalCompositeOperation = "source-over";
    ctx.closePath();
}

function drawRectangle(ctx, x1, y1, x2, y2){
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y1);
    ctx.lineTo(x2, y2);
    ctx.lineTo(x1, y2);
    ctx.lineTo(x1, y1);
    CheckBox();
}

function drawTriangle(ctx, x1, y1, x2, y2){
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.lineTo(x1-(x2-x1), y2);
    ctx.lineTo(x1, y1);
    CheckBox();
}

function drawEllipseByCenter(ctx, cx, cy, w, h) {
    drawEllipse(ctx, cx - w/2.0, cy - h/2.0, w, h);
}   

function drawEllipse(ctx, x, y, w, h) {
    var kappa =.5522848,
    ox = (w/2) * kappa,//control point offset horizontal
    oy = (h/2) * kappa,//control point offset vertical
    xe = x + w,//x-end
    ye = y + h,//y-end
    xm = x + w/2,//x-middle
    ym = y + h/2;//y-middle
    ctx.beginPath();
    ctx.moveTo(x, ym);
    ctx.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
    ctx.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
    ctx.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
    ctx.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
    CheckBox();  
}

function Redo(){
    if(redo.length!=0){
        var temp = redo.pop();
        undo.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
        ctx.putImageData(temp, 0, 0);
    }
}

function Undo(){
    if(undo.length!=0){
        var temp = undo.pop();
        redo.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
        ctx.putImageData(temp, 0, 0);
    }
}

function ChangeState(now){
    state = now;
    if(state=="type"){
        document.getElementById("canvas").className = "text";
    }
    else if(state=="circle" || state=="triangle" || state=="rectangle"){
        document.getElementById("canvas").className = "crosshair";
    }
    else if(state=="pencil"){
        document.getElementById("canvas").className = "pencil";
    }
    else if(state=="eraser"){
        document.getElementById("canvas").className = "eraser";
    }
    else if(state=="rainbow"){
        document.getElementById("canvas").className = "rainbow";
    }
    else{
        document.getElementById("canvas").className = "default";
    }
}

function CheckBox(){
    var myswitch = document.getElementById('check');
    ChangePencilArribute();
    if (myswitch.checked)ctx.fill();
    else ctx.stroke();
}

function ChangeWordAttribute(){
    word_size = document.getElementById("wordsize").value;
    word_form = document.getElementById("wordform").value;
    ctx.fontf = word_form;
    ctx.font = word_size + "px " + word_form;
    ctx.fillStyle = document.getElementById("choose").value;
    ChangePencilArribute();
}

function ChangePencilArribute(){
    ctx.strokeStyle = document.getElementById("choose").value;
    ctx.fillStyle = document.getElementById("choose").value;
    ctx.lineWidth = pen_weight;
}

function ChangePencilRange(){pen_weight = document.getElementById("PencilRange").value;}

function ChangeEraseRange(){erase_weight = document.getElementById("EraseRange").value;}

/*Clear*/
function clearall(){
    console.log("Clear!");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

/*Upload*/
addEventListener("change", ev=>{
    console.log("Read!");
    var file = ev.target.files[0];
    var reader  = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = function (e) {
        var image = new Image();
        image.src = e.target.result;
        image.onload = function(ev) {
           ctx.drawImage(image,0,0);
           
       }
    }
    undo.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    redo=[];
});


